


export class DataService {



    get(url) {
        return fetch(`${url}`, {method: "GET"})
            .then(this.processResponse)

    }


    processResponse(response) {
        return new Promise((resolve, reject) => {
            // will resolve or reject depending on status, will pass both "status" and "data" in either case
            let func;
            response.status < 400 ? func = resolve : func = reject;
            response.json().then(data => func(data)).catch(data => func(data));
        });
    }


}

