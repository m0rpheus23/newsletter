import * as React from "react";
import "./Spinner.css";

export class Spinner extends React.Component {
  render() {
    return (
      <div>
        <div
          className={
            "text spinner " +
            (this.props.isInverse ? " inverse" : "") +
            (this.props.isSmall ? " small" : "")
          }
        >
          {this.props.spinnerText}
        </div>
      </div>
    );
  }
}
