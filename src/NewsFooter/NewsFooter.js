import * as React from "react/cjs/react.development";
import './NewsFooter.css';


export default class NewsFooter extends React.Component {
    render() {
        return (
            <div className="NewsFooter-wrapper">
                <div className="NewsFooter-top">
                    <div className="NewsFooter-top-header">2017 Achievements</div>

                    <div className="NewsFooter-top-items">
                        <div className="NewsFooter-top-item">
                            <div className="header">
                                51,000
                            </div>
                            <div className="content">
                                <p><strong>Bab Rizq Jameel Recruitment:</strong>
                                    more than 51,000 job opportunities in 2017
                                </p>
                            </div>
                        </div>

                        <div className="NewsFooter-top-item">
                            <div className="header">
                                6,000
                            </div>
                            <div className="content">
                                <p><strong>Bab Rizq Jameel Recruitment:</strong>
                                    more than 6,000 job opportunities in 2017
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="NewsFooter-top-items">

                        <div className="NewsFooter-top-item">
                            <div className="header">
                                300<sup>mln</sup>
                            </div>
                            <div className="content">
                                <p><strong>Abdul Lateef Jameel Poverty Action Lab (J-PAL):</strong>
                                    over 300 million have been reached
                                </p>
                            </div>
                        </div>

                        <div className="NewsFooter-top-item">
                            <div className="header">
                                7,000
                            </div>
                            <div className="content">
                                <p><strong>Abdul Lateef Jameel Hospital:</strong>
                                    has served more than 7000 patients in 2017
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="NewsFooter-top-item">
                        <div className="header">
                            180
                        </div>
                        <div className="content">
                            <p><strong>Abdul Latif Jameel Toyota Endowed Scholarship:</strong> we had helped 180
                                students from 27 countries to study at MIT since establishment
                            </p>
                        </div>
                    </div>
                </div>
                <div className="NewsFooter-bottom">
                    <div className="flaticon-left-quotes-sign sign"></div>
                    <div className="highlight">
                        Check out The Daily Edition for the biggest stories
                        in news, business, tech, sports and entertainment.
                    </div>
                    <button className="download-btn">Download Issue</button>
                </div>
            </div>
        )
    }
}