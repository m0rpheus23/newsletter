import * as React from "react";
import "./NewsItem.css";


export default class NewsItem extends React.Component {


    render() {
        const {newsItem} = this.props;

        return (
            <div className="NewsItem-wrapper">
                <div className="NewsItem-header">
                    {newsItem.title}
                </div>
                <div className="NewsItem-image">
                    {newsItem.url &&
                    <img src={newsItem.url} alt={newsItem.title}/>
                    }
                </div>
                <div className="NewsItem-meta">
                    <div className="NewsItem-read-time">3 min read</div>
                    <div className="NewsItem-social">
                        <span className="flaticon-facebook"></span>
                        <span className="flaticon-twitter"></span>
                    </div>
                </div>
                <div className={newsItem.content ? "NewsItem-content" : 'NewsItem-content no-content'}>
                    {newsItem.content}
                    <span className="NewsItem-more flaticon-right-arrow"/>
                </div>
            </div>
        )
    }
}
