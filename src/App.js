import React, {Component} from 'react';
import './App.css';
import NewsItem from "./NewsItem/NewsItem";
import AppService from "./App.service";
import NewsFooter from "./NewsFooter/NewsFooter";
import {Spinner} from "./shared/components/Spinner/Spinner";


class App extends Component {


    constructor(props) {
        super(props);

        this.appService = new AppService();

        this.state = {
            newsItems: [],
            isLoading: false
        }

    }

    componentDidMount() {
        this.setState({
            isLoading: true
        });

        this.appService.getNews().then((response) => {
            this.setState({
                newsItems: response,
                isLoading: false
            })
        })
    }

    render() {
        const {newsItems, isLoading} = this.state;
        const newsItemList = newsItems.map((newsItemCategory, index) => (
            <div key={index}>
                <div className="NewsItem-category">{newsItemCategory.category}</div>
                {newsItemCategory['data'].map(newsItem => (
                    <NewsItem newsItem={newsItem} key={newsItem.id}/>
                ))}
            </div>
        ))

        return (
            <div className="App-wrapper">
                <div className="App">
                    <header className="App-header">
                        <div className="App-date">January 25, 2018</div>
                        <h1 className="App-title">Newsletter Winter Issue</h1>
                    </header>

                    <section className="App-content">
                        {isLoading &&
                        <Spinner spinnerText={'Getting News Items'}/>
                        }
                        {!isLoading &&
                        (newsItemList)
                        }
                    </section>
                    <section className="App-footer">
                        <NewsFooter/>
                    </section>
                </div>
                <div className="App-social">
                    <div className="header">Follow Us</div>
                    <div className="icons">
                        <div className="flaticon-facebook"></div>
                        <div className="flaticon-twitter"></div>
                        <div className="flaticon-google-plus"></div>
                        <div className="flaticon-youtube-logo"></div>
                    </div>
                </div>
                <div className=" App-outer">

                    <p>
                        For more information <a href="">click here</a>, or to speak with one of our support
                        representatives,
                        please
                        call us on +9662 6603177 (KSA) or +9714 4480800 Ext 0962 (Dubai).
                    </p>
                    <p>All Rights Reserved 2017 </p>
                    <p>Disclaimer: You are receiving this email as a privileged customer of Royal Jet. You may,
                        at any time, choose not to receive e-mail marketing literature/information about
                        our program or services by <a href="">clicking here</a> to unsubscribe.
                    </p>
                </div>
            </div>
        );
    }
}

export default App;
