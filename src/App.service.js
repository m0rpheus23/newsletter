import {DataService} from "./shared/services/data.service";


export default class AppService {

    constructor() {
        this.dataService = new DataService();
    }

    getNews() {
        return this.dataService.get('https://www.amock.io/api/m0rpheus/data')
    }
}